import mysql from 'mysql';

const pool = mysql.createPool(require('./../dbconfig'));

function query(sql,params,callback){
    pool.getConnection(function(err,conn){
        if(!err){
            conn.query(sql,params,callback);
            conn.release();
        }else{
            callback(err,null,null);
        }
    })
}

module.exports = {query};