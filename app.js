import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cookieSession from 'cookie-session';
import uuid from 'uuid';
import consolidate from 'consolidate';
import multer from 'multer';
import logger from 'morgan';

//配置上传文件的中间件
let multerObj = multer({dest:'./public/upload'});
let app = express();
app.listen(8080);

//中间件的配置
app.use(express.static('./public'));
app.use(multerObj.any());//容许所有的文件上传
//日志
app.use(logger('dev'));
app.use(logger('combined',{stream:require('fs').createWriteStream('./logs/logs.log',{flags:'a'})}));
//设置post请求的中间件
app.use(bodyParser.urlencoded({extended:true,limit:'500mb'}));

app.use(bodyParser.json());
app.use(bodyParser.text());

app.use(cookieParser('itqian'));
app.use(cookieSession({
    genid:function(){
        return uuid.v1();
    },
    maxAge:20*1000*60,
    secret:'itqian'
}))
//模板引擎
app.set('view engine','html');
app.set('views','./views');
app.engine('html',consolidate.ejs);


//路由
app.use('/admin',require('./route/admin/adminRouter'));

